<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Product;
use Illuminate\Http\Request;

class ProductBuyerController extends ApiController
{
    /**
     * ProductBuyerController constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        //
        $buyers = $product->transactions()
            //->whereHas('buyers')
            ->with('buyer')
            ->get()
            ->pluck('buyer')
            //->collapse()
            //->values()
            ->unique('id')
            ->values();

        return $this->showAll($buyers);
    }
}
