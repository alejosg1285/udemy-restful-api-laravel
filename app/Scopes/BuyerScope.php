<?php
    /**
     * Created by PhpStorm.
     * User: Alejandro Saa Garcia
     * Date: 20/03/2019
     * Time: 5:52 AM
     */

    namespace App\Scopes;


    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Scope;

    class BuyerScope implements Scope
    {
        public function apply(Builder $builder, Model $model) {
            // TODO: Implement apply() method.
            $builder->has('transactions');
        }
    }