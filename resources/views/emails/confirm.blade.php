@component('mail::message')
    # Hallo {{$user->name}}

    You changed your e-mail, so we need to verify this new address. Please verify your e-mail using this button:

    @component('mail::button', ['url' => route('verify', $user->verification_token)])
        Verify account
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent